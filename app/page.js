'use client'
import Image from 'next/image'
import { UserAuth } from "./context/AuthContext";
export default function Home() {
  const { user } = UserAuth();
  console.log(user)
  return (
    <main className="p-4">
      <h1>Token:</h1>
      <h1>{user.accessToken}</h1>
    </main>
  )
}
